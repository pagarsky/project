import os
import tempfile
import string
import random
import datetime

import pytest
import flask_migrate as migrate


db_file, os.environ['DB_URI'] = tempfile.mkstemp()  # override db


import departments_app
from database.models import Employee, Department
import database.database


def insert_test_data(session):
    dep1 = Department(title="first_test")
    dep2 = Department(title="second_test")

    employee1 = Employee(
        name='first_test',
        date_of_birth=datetime.date.today(),
        salary=random.randint(100, 1000),
        department_id=1)

    employee2 = Employee(
        name='second_test',
        date_of_birth=datetime.date.today(),
        salary=random.randint(100, 1000),
        department_id=2)

    session.add(dep1)
    session.add(dep2)
    session.add(employee1)
    session.add(employee2)

    session.commit()


@pytest.fixture
def client():
    db_fd, departments_app.app.config['DATABASE'] = tempfile.mkstemp()
    departments_app.app.config['TESTING'] = True

    with departments_app.app.test_client() as client:
        with departments_app.app.app_context():
            migrate.upgrade()
            database.database.Base.metadata.create_all(bind=database.database.engine)
            insert_test_data(database.database.Session)
        yield client

    os.close(db_fd)
    os.unlink(departments_app.app.config['DATABASE'])
