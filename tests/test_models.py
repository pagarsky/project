import database.models as models


def test_department_repr(client):  # not very useful test, but i want 100% coverage for models.py
    department = models.Department(title='Test_dep')

    assert repr(department) == f'Department {department.id}'
