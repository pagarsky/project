import json
import datetime


def test_employee_get(client):
    req = client.get('/api/employees/1')

    assert req.status_code == 200
    assert req.headers['Content-Type'] == 'application/json'


def test_employee_get_404(client):
    req = client.get('/api/employees/404')

    assert req.status_code == 404
    assert b'Not Found' in req.data


def test_employee_put(client):
    req = client.put(
        '/api/employees/1',
        data=json.dumps({'name': 'someone'}),
        headers={'Content-Type': 'application/json'}
    )

    assert req.status_code == 200
    assert req.json['name'] == 'someone'


def test_employee_put_404(client):
    req = client.put(
          '/api/employees/1000',
          data=json.dumps({'name': 'someone'}),
          headers={'Content-Type': 'application/json'}
    )

    assert req.status_code == 404


def test_employee_delete(client):
    req = client.delete('api/employees/1')

    assert req.status_code == 200


def test_employee_delete_404(client):
    req = client.delete('/api/employees/404')

    assert req.status_code == 404


def test_employee_serialized_correctly(client):
    req = client.get('/api/employees/2')

    assert req.status_code == 200
    assert type(req.json['salary']) is str


def test_employees_get(client):
    req = client.get("/api/employees")

    assert req.status_code == 200
    assert req.headers['Content-Type'] == 'application/json'


def test_employees_post(client):
    employee_json = json.dumps({
        'name': 'someone',
        'department_id': 0,
        'date_of_birth': str(datetime.date.today()),
        'salary': 1200
    })
    req = client.post('/api/employees', data=employee_json, headers={'Content-Type': 'application/json'})

    assert req.status_code == 201
    assert set(req.json) - set(json.loads(employee_json)) == {'id'}


def test_department_get(client):
    req = client.get('/api/departments/1')

    assert req.status_code == 200
    assert req.headers['Content-Type'] == 'application/json'


def test_department_get_404(client):
    req = client.get('/api/departments/404')

    assert req.status_code == 404


def test_department_put(client):
    req = client.put(
        '/api/departments/1',
        data=json.dumps({'title': 'some_other'}),
        headers={'Content-Type': 'application/json'}
    )

    assert req.status_code == 200
    assert req.json['title'] == 'some_other'


def test_department_put_404(client):
    req = client.put(
        '/api/departments/404',
        data=json.dumps({'title': 'some_other'}),
        headers={'Content-Type': 'application/json'}
    )

    assert req.status_code == 404


def test_department_delete(client):
    req = client.delete('/api/departments/1')

    assert req.status_code == 200


def test_department_delete_404(client):
    req = client.delete('/api/departments/404')

    assert req.status_code == 404


def test_departments_get(client):
    req = client.get('/api/departments')

    assert req.headers['Content-Type'] == 'application/json'
    assert req.status_code == 200


def test_departments_post(client):
    req = client.post(
        '/api/departments',
        data=json.dumps({'title': 'some_other', 'employees': []}),
        headers={'Content-Type': 'application/json'}
    )

    assert req.status_code == 201
    assert req.json['title'] == 'some_other'
