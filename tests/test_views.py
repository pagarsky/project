def test_index_view_redirect(client):
    req = client.get('/')

    assert req.status_code == 302


def test_employees_view(client):
    req = client.get('/employees')

    assert req.status_code == 200


def test_employees_view_post(client):
    req = client.post(
        '/employees',
        data={
            'name': 'test_post_employee',
            'department': 1
        },
    )

    assert req.status_code == 200
    assert b'test_post_employee' in req.data


def test_employee_view(client):
    req = client.get('/employees/2')

    assert req.status_code == 200
    assert b'second_test' in req.data


def test_employee_view_redirect(client):
    req = client.get('/employees/')

    assert req.status_code == 302


def test_employee_view_404(client):
    req = client.get('/employees/404')

    assert req.status_code == 404


def test_departments_view(client):
    req = client.get('/departments')

    assert req.status_code == 200


def test_departments_view_post(client):
    req = client.post('/departments', data={'title': 'some'})

    assert req.status_code == 200


def test_department_view(client):
    req = client.get('/departments/3')

    assert req.status_code == 200


def test_department_view_redirect(client):
    req = client.get('/departments/')

    assert req.status_code == 302


def test_department_view_404(client):
    req = client.get('/departments/404')

    assert req.status_code == 404


def test_department_edit_view(client):
    req = client.get('/departments/2/edit')

    assert req.status_code == 200


def test_department_edit_view_post_redirect(client):
    req = client.post('departments/2/edit', data={'name': 'test_name_something'})

    assert req.status_code == 302  # redirect to '/departments'


def test_department_edit_view_404(client):
    req = client.get('/departments/404/edit')

    assert req.status_code == 404


def test_employee_edit_view(client):
    req = client.get('/employees/2/edit')

    assert req.status_code == 200


def test_employee_edit_post(client):
    req = client.post('/employees/2/edit', data={'name': 'another_test_name'})

    assert req.status_code == 302  # redirect to '/employees'


def test_employee_edit_view_404(client):
    req = client.get('/employees/404/edit')

    assert req.status_code == 404
