FROM python:3.9-slim-buster

RUN apt-get -y update && apt-get -y upgrade

COPY . .

RUN python -m pip install -r requirements.txt

ENV FLASK_APP="run"
ENV INSTANCE_CONNECTION_NAME="cloud-computing-370220:europe-central2:cc-mysql-instance"
ENV DB_USER="root"
ENV DB_PASS="unroot"
ENV DB_NAME="cc-mysql-instance"

VOLUME ./

# RUN chmod -x run.py
# CMD ["flask", "run", "--host=0.0.0.0"]

RUN chmod u+x ./entrypoint.sh
ENTRYPOINT ["./entrypoint.sh"]
