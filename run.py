#!/usr/bin/env python3
from departments_app import app, db


if __name__ == '__main__':
    db.create_all()
    app.run()
