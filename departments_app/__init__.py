import string
import random

from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from database.database import engine


app = Flask(__name__, instance_relative_config=True)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

try:
    app.config.from_pyfile('config.py')
except FileNotFoundError:
    app.config.from_object('config')

app.config.update(
    SECRET_KEY=''.join(random.choices(string.ascii_letters, k=32))
)


import departments_app.views
import departments_app.rest.schemas     # initialize marshmallow(ma)
import departments_app.rest.api         # initialize api(api)
