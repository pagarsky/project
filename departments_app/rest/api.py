# from typing import List, Dict
# import datetime

# from flask_restful import Api, Resource, request

# from departments_app import app
# from departments_app.rest.schemas import employee_schema, employees_schema, department_schema, departments_schema
# from database.models import Department, Employee
# from database.database import Session


# api = Api(app)


# def make_serializable(employee):
#     """Modify employee or list of employees by converting 'salary' field to str."""
#     if isinstance(employee, list):
#         for emp in employee:
#             emp['salary'] = str(emp.get('salary', 0))
#         return employee
#     else:
#         employee['salary'] = str(employee.get('salary', 0))
#         return employee


# class EmployeeResource(Resource):

#     def get(self, id: int):
#         """Return employee in JSON."""
#         employee = Employee.query.get(id)

#         if employee is None:
#             return 'Not Found', 404

#         return make_serializable(employee_schema.dump(employee)), 200

#     def put(self, id: int):
#         """Modify employee and return it in JSON."""
#         employee = Employee.query.get(id)

#         if employee is None:
#             return "Not Found", 404

#         for field in request.json:
#             setattr(employee, field, request.json[field])

#         Session.commit()
#         return make_serializable(employee_schema.dump(employee)), 200

#     def delete(self, id: int):
#         """Delete specified employee and return 200 status code."""
#         employee = Employee.query.get(id)

#         if employee is None:
#             return 'Not Found', 404

#         Session.delete(employee)
#         Session.commit()

#         return 'OK', 200


# class EmployeesResource(Resource):

#     def get(self):
#         """Return list of all employees in JSON."""
#         return make_serializable(employees_schema.dump(Employee.query.all())), 200

#     def post(self):
#         """Add new employee and return it in JSON."""
#         employee = Employee(
#             name=request.json['name'],
#             department_id=request.json['department_id'],
#             date_of_birth=datetime.datetime.fromisoformat(request.json['date_of_birth']),
#             salary=request.json['salary']
#         )

#         Session.add(employee)
#         Session.commit()

#         return make_serializable(employee_schema.dump(employee)), 201


# class DepartmentResource(Resource):

#     def get(self, id: int):
#         """Return department in JSON."""
#         department = Department.query.get(id)

#         if department is None:
#             return 'Not Found', 404

#         return department_schema.dump(department)

#     def put(self, id: int):
#         """Update specified department and return it in JSON."""
#         department = Department.query.get(id)

#         if department is None:
#             return 'Not Found', 404

#         for field in request.json:
#             setattr(department, field, request.json[field])

#         Session.commit()
#         return make_serializable(department_schema.dump(department)), 200

#     def delete(self, id: int):
#         """Delete specified department and return 200 status code."""
#         department = Department.query.get(id)

#         if department is None:
#             return 'Not Found', 404

#         Session.delete(department)
#         Session.commit()

#         return 'OK', 200


# class DepartmentsResource(Resource):

#     def get(self):
#         """Return list of all departments in JSON."""
#         return departments_schema.dump(Department.query.all()), 200

#     def post(self):
#         """Add department and return it in JSON."""
#         dep = Department(title=request.json['title'])
#         employees = Employee.query.filter(Employee.id.in_(request.json['employees'])).all()
#         dep.employees = employees

#         Session.add(dep)
#         Session.commit()

#         return department_schema.dump(dep), 201


# api.add_resource(EmployeesResource, '/api/employees')
# api.add_resource(EmployeeResource, '/api/employees/<int:id>')

# api.add_resource(DepartmentsResource, '/api/departments')
# api.add_resource(DepartmentResource, '/api/departments/<int:id>')
