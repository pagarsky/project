from flask_marshmallow import Marshmallow

from departments_app import app
from database.models import Employee, Department


ma = Marshmallow(app)


class EmployeeSchema(ma.SQLAlchemyAutoSchema):
    """Marshmallow schema class to serialize employees."""
    class Meta:
        model = Employee
        include_fk = True


class DepartmentSchema(ma.SQLAlchemyAutoSchema):
    """Marshmallow schema class to serialize departments."""
    class Meta:
        model = Department
        include_fk = True


employee_schema = EmployeeSchema()
employees_schema = EmployeeSchema(many=True)

department_schema = DepartmentSchema()
departments_schema = DepartmentSchema(many=True)
