import datetime
from flask import render_template, redirect, request
from database.models import Department, Employee
from sqlalchemy.sql import func, and_

from departments_app.forms import create_department_form, create_employee_form, FilterEmployeeForm
from departments_app import app


@app.route('/')
def index():
    """Obligatory index page which is redirects to departments."""
    return redirect('/departments')


@app.route('/employees', methods=['GET', 'POST'])
def employees():
    """Return page with all employees list."""
    add_form = create_employee_form(request, None)
    filter_form = FilterEmployeeForm(request.form)
    employees = None

    if request.method == 'POST':
        if add_form.submit.data and add_form.validate_on_submit():
            name = add_form.name.data
            date = add_form.birth_date.data if add_form.birth_date.data else datetime.date.today()
            salary = add_form.salary.data if add_form.salary.data else 0

            Session.add(Employee(name=name, department_id=add_form.department.data, date_of_birth=date, salary=salary))
            Session.commit()

        if filter_form.filter.data and filter_form.validate_on_submit():
            start, end = filter_form.from_.data, filter_form.to_.data
            employees = Employee.query\
                .filter(and_(start <= Employee.date_of_birth, Employee.date_of_birth <= end))\
                .all()
            print(employees)

    if employees is None:
        employees = Employee.query.all()

    return render_template('employees/employees.html', add_form=add_form, filter_form=filter_form, employees=employees)


@app.route('/employees/')
@app.route('/employees/<int:employee_id>')
def employee(employee_id=None):
    """Return page with one employee description."""
    if employee_id is None:
        return redirect('/employees')

    employee = Employee.query.get(employee_id)

    if not employee:
        return f'Employee with id={employee_id} does not exist', 404

    return render_template('employees/employee.html', id=employee_id, employee=employee)


@app.route('/departments', methods=['GET', 'POST'])
def departments():
    """Return page with list of all departments."""
    form = create_department_form(request, None)

    if request.method == 'POST':
        title = form.title.data

        if title:
            dep = Department(title)
            update_department(dep, form)

    objects = Session.query(Department, func.avg(Employee.salary))\
        .group_by(Department.id)\
        .outerjoin(Employee)\
        .all()

    departments = [obj[0] for obj in objects]
    salaries = [obj[1] if obj[1] is not None else 0 for obj in objects]

    return render_template('departments/departments.html', add_department_form=form, departments=departments, salaries=salaries, zip=zip)


@app.route('/departments/')
@app.route('/departments/<int:department_id>')
def department(department_id=None):
    """Return page with department description."""
    if department_id is None:
        return redirect('/departments')

    department = Department.query.get(department_id)

    if not department:
        return f'Employee with id={department_id} does not exist', 404

    return render_template('departments/department.html', id=department_id, department=department)


def update_department(department, form):
    """Update department in db and commit changes."""
    ids = set(int(i) for i in form.employees.data)
    department.employees += Employee.query.filter(Employee.id.in_(ids)).all()
    Session.add(department)
    Session.commit()


def update_employee(employee, form):
    """Update employee in db and commit changes."""
    employee.name = form.name.data
    employee.date_of_birth = form.birth_date.data if form.birth_date.data else datetime.date.today()
    employee.salary = form.salary.data
    employee.department_id = form.department.data

    Session.add(employee)
    Session.commit()


@app.route('/departments/<int:dep_id>/edit', methods=['GET', 'POST'])
def department_edit(dep_id=None):
    """Return department edit page with edition form."""
    department = Department.query.get(dep_id)
    edit_form = create_department_form(request, department)

    if not department:
        return f'Department with id={dep_id} does not exist', 404

    if request.method == 'POST':
        update_department(department, edit_form)
        return redirect('/departments/')

    form = create_department_form(request, department)
    return render_template('departments/department_edit.html', id=dep_id, department=department, form=form)


@app.route('/employees/<int:emp_id>/edit', methods=['GET', 'POST'])
def employee_edit(emp_id=None):
    """Return employee edit page with edition form."""
    employee = Employee.query.get(emp_id)
    edit_form = create_employee_form(request, employee)

    if not employee:
        return f'Department with id={emp_id} does not exist', 404

    if request.method == 'POST':
        update_employee(employee, edit_form)
        return redirect('/employees/')

    return render_template('employees/employee_edit.html', id=emp_id, employee=employee, form=edit_form)
