from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SelectMultipleField, SubmitField, DateField, DecimalField
from wtforms.validators import DataRequired, Length

from database.models import Department, Employee


def create_department_form(request, department):
    """Return DepartmentForm with dynamically created choices."""
    form = DepartmentForm(request.form, obj=department)
    form.employees.choices = [(emp.id, emp.name) for emp in Employee.query.order_by('name')]

    return form


def create_employee_form(request, employee):
    """Return EmployeeForm with dynamically created choices."""
    form = EmployeeForm(request.form, obj=employee)
    form.department.choices = [(dep.id, dep.title) for dep in Department.query.order_by('title')]

    return form


class DepartmentForm(FlaskForm):
    """Flask Form for department object."""
    title = StringField('Title', validators=[DataRequired(), Length(min=3, max=50)])
    employees = SelectMultipleField('Employees')

    submit = SubmitField('Save')


class EmployeeForm(FlaskForm):
    """Flask Form for employee object."""
    name = StringField('Name', validators=[DataRequired(), Length(min=3, max=50)])
    birth_date = DateField('Date Of Birth')
    salary = DecimalField('Salary', validators=[DataRequired()])
    department = SelectField('Department')

    submit = SubmitField('Save')


class FilterEmployeeForm(FlaskForm):
    """Flask form for date filter at /employees page."""
    from_ = DateField('From')
    to_ = DateField('To')

    filter = SubmitField('Filter')
