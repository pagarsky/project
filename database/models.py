from sqlalchemy import Column, Integer, String, Date, Numeric, ForeignKey
from sqlalchemy.orm import relationship
from departments_app import db


class Employee(db.Model):
    """Employee db model."""
    __tablename__ = 'employees'

    id = Column(Integer, primary_key=True)
    department_id = Column(Integer, ForeignKey('departments.id'))
    name = Column(String(50), unique=False)
    date_of_birth = Column(Date())
    salary = Column(Numeric(precision=None, scale=2))

    def __init__(self, name, department_id, date_of_birth, salary):
        self.name = name
        self.department_id = department_id
        self.date_of_birth = date_of_birth
        self.salary = salary

    def __repr__(self):
        return f'<Employee {self.id}>'


class Department(db.Model):
    """Department db model."""
    __tablename__ = 'departments'

    id = Column(Integer, primary_key=True)
    title = Column(String(50))
    employees = relationship('Employee', backref='departments', cascade='all, delete-orphan')

    def __init__(self, title):
        self.title = title

    def __repr__(self):
        return f'Department {self.id}'
