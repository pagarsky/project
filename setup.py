from setuptools import setup

setup(
    name='departments_app',
    packages=['departments_app'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)
